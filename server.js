const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const os = require('os');
const pino = require('express-pino-logger')()

const port = process.env.PORT || 80;

app.use(pino)
app.use(bodyParser.json());

app.get("/", (req, res) => {
  const hostname = os.hostname();
  const load = os
    .loadavg()
    .map(avg => Math
      .round(avg * 100) / 100);

  return res.status(200).send({
    time: new Date().toUTCString(),
    hostname,
    load,
  });
});

app.listen(port, () => {
  console.log("Listening on " + port);
});
