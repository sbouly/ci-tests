===============
Chapter 1
===============
Section 1.1
------------------
1. Use this format the numbers in your list like 1., 2., etc.

A. To make items in your list go like A., B., etc. Both uppercase and lowercase letters are acceptable.

I. Roman numerals are also acceptable, uppercase or lowercase.

(1) Numbers in brackets are acceptable.

1) So are numbers followed by a bracket.

* Bullet point

  - nested bullet point

    + even more nested bullet point

This is the paragraph preceding the code sample::

  #some sample code